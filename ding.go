package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	fmt.Println("Ding is running...")
	fmt.Println("Open http://127.0.0.1:7088 in browser")
	http.HandleFunc("/check", func(w http.ResponseWriter, r *http.Request) {
		_, err := http.Get("http://example.com/")
		if err != nil {
			fmt.Fprint(w, "No Internet Connection")
		}
	})
	http.Handle("/", http.FileServer(http.Dir("resources/")))
	err := http.ListenAndServe(":7001", nil)
	if err != nil {
		log.Fatal(err)
	}
}
